**Script de construction d'une PrimTux7 buster sur Raspberry Pi**

Ce script permet de construire une distribution PrimTux7-Debian10 pour le nano ordinateur Raspberry Pi à partir de Raspberry Pi OS lite.

Testé avec :

* 021-05-07-raspios-buster-armhf-lite.img

*IMPORTANT : une carte d'au moins 32 Go est nécessaire pour cette construction.*
 
De nombreux paramètres peuvent empêcher le bon déroulement du script (coupure Internet, dépôts inaccessibles, problème de désarchivage, etc.). Dans ce cas, le script peut être relancé autant de fois que nécessaire.

## Utilisation :

Le Raspberry Pi doit être démarré avec une carte SD sur laquelle est installé Raspberry Pi OS lite.

Connectez-vous avec le login par défaut:

```
pi
```

Mot de passe par défaut de Raspberry Pi OS:

```
raspberry
```

ATTENTION: par défaut Raspberry Pi OS est configuré avec un clavier anglo-saxon. Il faudra en tenir compte lors des saisies. Pour le mot de passe, avec un clavier AZERTY, il faut saisir `rqspberry`.

Configurez les paramètres de localisation, de clavier, de WiFi à l'aide de l'utilitaire inclus raspi-config

```
sudo raspi-config
```

(sudo `rqspi)config` avec un clavier AZERTY)

Activez le compte root en lui attribuant un mot de passe:

```
sudo passwd root
```

Je vous invite à saisir `tuxprof` comme mot de passe, car c'est celui proposé par défaut dans PrimTux. Vous pourrez toujours le changer par la suite.

Redémarrez puis connectez-vous sous le compte root.

```
wget https://www.primtux.fr/Documentation/armhf/install-ptx7-buster-rpi.sh

chmod +x install-ptx7-buster-rpi.sh

./install-ptx7-buster-rpi.sh
```

L'opération nécessite plusieurs heures, et dépend de la qualité de votre liaison Internet.

L'installation de Samba se fait de façon interactive et nécessite de répondre à une question. Il sera également demandé le gestionnaire de sessions à utiliser. Il suffit de valider le choix de sddm par défaut.

Redémarrez en fin d'opération. 

Le script crée un fichier de déroulement des opérations en `/var/log/install-primtux-rpi.log`
