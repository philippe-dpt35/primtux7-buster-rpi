z#!/bin/bash

############################################
# Script de construction d'une PrimTux 7   #
# sur Raspberry Pi disposant d'une         #
# Raspberry Pi OS lite buster installée.   #
# Auteur: Philippe Ronflette               #
# philippe.dpt35@yahoo.fr                  #
############################################

debut=$(date +%s)

# Paramétrage des sources, à modifier si nécessaire
sources="https://framagit.org/Steph/primtux7/-/raw/main/RPI/includes.chroot.tar.gz https://framagit.org/Steph/primtux7/-/raw/main/RPI/hooks.tar.gz"
sources_jlodb="https://framagit.org/Steph/primtux7/-/raw/9a115352e4bc1188a189886b818b56b8cc02177a/Debian11-amd64/config/packages/"
config_xfce="https://www.primtux.fr/Documentation/armhf/xfce4-config.tar.gz"
config_roxterm="https://www.primtux.fr/Documentation/armhf/Profiles.tar"
config_audacity="https://primtux.fr/Documentation/armhf/audacity-data.tar.gz"
archive_chroot="includes.chroot.tar.gz"
archive_hooks="hooks.tar.gz"
archive_xfce="xfce4-config.tar.gz"
archive_audacity="audacity-data.tar.gz"


version="PrimTux7 Debian10 RPi"

# Listes des paquets à installer
# paquets supprimés en cours d'élaboration du script : oxygen-icon-theme breeze-icon-theme arandr genisoimage fluxbox gigolo hardinfo pcmanfm
paquets_base="accountsservice acl acpid adduser adwaita-icon-theme anacron ant apparmor aptitude aptitude-common aria2 aspell aspell-fr at-spi2-core audacity avahi-autoipd baobab blt ca-certificates-java catfish cdparanoia cpulimit cups-client cups-common cups-pk-helper dbus-user-session dbus-x11 dconf-cli dconf-service debian-reference-common default-jre default-jre-headless desktop-base desktop-file-utils dictionaries-common dns323-firmware-tools eject enchant evince exfat-fuse exfat-utils expeyes-firmware-dev faenza-icon-theme fbterm ffmpeg ffmpegthumbnailer file-roller filezilla filezilla-common firefox-esr firefox-esr-l10n-fr firmware-microbit-micropython firmware-microbit-micropython-doc firmware-samsung flac font-manager fontconfig fontconfig-config fonts-dejavu fonts-dejavu-core fonts-dejavu-extra fonts-droid-fallback fonts-freefont-ttf fonts-liberation fonts-liberation2 fonts-opendyslexic fonts-opensymbol fonts-sil-andika fonts-sil-gentium fonts-sil-gentium-basic fonts-wine fotowall frei0r-plugins fskbsetting fxload galternatives gdebi gdebi-core geany geany-common geoclue-2.0 gnome-desktop3-data gnome-font-viewer gnome-getting-started-docs gnome-icon-theme gnome-icon-theme-symbolic gnome-keyring gnome-screenshot gnome-system-tools gparted gpicview gsettings-desktop-schemas gsfonts gsfonts-x11 gtk-update-icon-cache gtk2-engines gucharmap gvfs-backends gvfs-common gvfs-daemons gvfs-fuse haveged hicolor-icon-theme hpijs-ppds hunspell-fr hunspell-fr-classical iio-sensor-proxy im-config imagemagick itools java-common java-wrappers javascript-common kde-l10n-fr kded5 kdenlive kdenlive-data kio konwert konwert-filters lame libgconf-2-4 libgtk3-perl libreoffice libreoffice-gtk3 libreoffice-help-fr libreoffice-l10n-fr libreoffice-report-builder libreoffice-style-tango libsnack-alsa libtk-img lightning lintian live-tools lp-solve lsof lv marble mate-calc media-player-info menu menulibre midisport-firmware mousepad mpv murrine-themes mysql-common mythes-fr network-manager network-manager-gnome nginx notification-daemon numix-blue-gtk-theme numix-gtk-theme numlockx nxt-firmware ofono onboard openboard onboard-common os-prober osmo papirus-icon-theme p7zip p7zip-full pavucontrol pdfsam photoflare plasma-framework pulseaudio pulseaudio-utils python-gtk2 python-tk python-tk rox-filer roxterm samba samba-common samba-common-bin sane-utils scribus scribus-data sddm sigrok-firmware-fx2lafw simple-scan smplayer smplayer-l10n smplayer-themes smtube sonnet-plugins sound-icons soundconverter speech-dispatcher speech-dispatcher-espeak-ng synaptic system-config-printer system-config-printer-common task-french task-french-desktop task-laptop tcl tcl8.6 tcl-snack time tix tk tk8.6 tk8.6-blt2.5 tk8.6-blt2.5 trash-cli ttf-aenigma ttf-dejavu ttf-dejavu-core ttf-dejavu-extra ttf-unifont update-inetd upower ure usbmuxd user-setup util-linux-locales uuid-runtime vlc wfrench wine winff winff-data winff-gtk2 x11-apps x11-common x11-session-utils x11-utils x11-xkb-utils x11-xserver-utils xbitmaps xbrlapi xdg-dbus-proxy xdg-utils xfburn xfce4 xfce4-appfinder xfce4-clipman xfce4-clipman-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin xfce4-dict xfce4-diskperf-plugin xfce4-fsguard-plugin xfce4-genmon-plugin xfce4-goodies xfce4-mailwatch-plugin xfce4-netload-plugin xfce4-notifyd xfce4-panel xfce4-places-plugin xfce4-power-manager xfce4-power-manager-data xfce4-power-manager-plugins xfce4-pulseaudio-plugin xfce4-screenshooter xfce4-sensors-plugin xfce4-session xfce4-settings xfce4-smartbookmark-plugin xfce4-systemload-plugin xfce4-taskmanager xfce4-terminal xfce4-timer-plugin xfce4-verve-plugin xfce4-wavelan-plugin xfce4-weather-plugin xfce4-whiskermenu-plugin xfce4-xkb-plugin xfdesktop4 xfconf xfonts-100dpi xfonts-75dpi xfonts-base xfonts-encodings xfonts-scalable xfonts-unifont xfonts-utils xinit xorg xorg-docs-core xournal xsane xsane-common xscreensaver xscreensaver-data xserver-common xserver-xorg xserver-xorg-core xserver-xorg-input-all xserver-xorg-input-libinput xserver-xorg-input-wacom xserver-xorg-legacy xserver-xorg-video-all xserver-xorg-video-fbdev xserver-xorg-video-nouveau xserver-xorg-video-qxl xserver-xorg-video-vesa xsettingsd xterm xxkb yelp yelp-xsl youtube-dl zenity zenity-common zip"

paquets_educatifs="abuledu-aller-primtux abuledu-associations-primtux abuledu-microtexte abuledu-minitexte achats alacampagne association-images association-images2 aujardin avoir-etre balance-virtuelle blocs-logiques chiffres-lettres croissant-decroissant digiscreen-primtux dizaines edit-interactive-svg encoder-mots gcompris-qt geogebra-classic goldendict gtans histoires jclic kgeography kgeography-data klettres klettres-data ktuberling kturtle leterrier-calculment leterrier-cibler leterrier-fubuki leterrier-tierce leximots lis-ecris lis-ecris2 multiplication-station-primtux omnitux-light openboard ordre-alphabetique pendu-peda-gtk qdictionnaire ri-li scratch toutenclic tuxpaint tuxpaint-config tuxpaint-plugins-default tuxpaint-stamps-default tuxmath"

paquets_primtux="accueil-primtux7 administration-eleves-primtux gspeech gtkdialog handymenu mothsart-wallpapers-primtux"

paquets_rpi="sauve-carte yad"

paquets_jlodb="poufpoufce1_19.10.0_all.deb poufpoufce2_19.10.0_all.deb poufpoufcm2_19.10.0_all.deb poufpoufcp_19.10.0_all.deb poufpoufinfo_19.10.0_all.deb poufpoufjeux_19.10.0_all.deb poufpoufxyz_19.10.0_all.deb"

# Redirection d'erreurs vers un fichier log
fichierlog="/var/log/install-primtux-rpi.log"
if ! [ -e "$fichierlog" ]
    then > "$fichierlog"
fi
echo "###############################################################################################
Lancement du script de construction" >> "$fichierlog"
date >> "$fichierlog"
exec 2>>"$fichierlog"

# Téléchargement des sources de configuration de PrimTux 7 buster
debut_telechargement=$(date +%s)
mkdir /tmp/sources-primtux
echo "Téléchargement des sources. Cela peut prendre du temps."
wget -P /tmp/sources-primtux $sources 2>&1 | tee -a "$fichierlog"
wget -P /tmp "$config_xfce" 2>&1 | tee -a "$fichierlog"
wget -P /tmp "$config_audacity" 2>&1 | tee -a "$fichierlog"
wget -P /tmp "$config_roxterm" 2>&1 | tee -a "$fichierlog"
if ! [ -e /tmp/sources-primtux/"$archive_chroot" ] || ! [ -e /tmp/sources-primtux/"$archive_hooks" ] || ! [ -e /tmp/"$archive_xfce" ]  || ! [ -e /tmp/"$archive_audacity" ]; then
  echo "Des fichiers archives essentiels n'ont pu être téléchargés.
Le script va s'arrêter.
Veuillez vérifier votre connexion Internet et relancer le script." | tee -a "$fichierlog"
  exit 1
fi
fin_telechargement=$(date +%s)
temps_telechargement=$(($fin_telechargement-$debut_telechargement))
temps_telechargement=$(echo $temps_telechargement |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
echo "Fin de téléchargement des fichiers sources
------------------------------------------" | tee -a "$fichierlog"

# Ajout des dépôts PrimTux
echo "deb https://depot.primtux.fr/repo/debs/ PrimTux7-armhf main" > /etc/apt/sources.list.d/primtux7.list
wget -O - https://depot.primtux.fr/repo/debs/key/PrimTux.gpg.key | apt-key add -

# Ajout du dépôt non libre
echo "deb https://deb.debian.org/debian buster non-free" >> /etc/apt/sources.list
wget -q https://ftp-master.debian.org/keys/release-10.asc -O- | apt-key add -

# Téléchargement des paquets libttspico
wget -N https://ftp.debian.org/debian/pool/non-free/s/svox/libttspico-utils_1.0+git20130326-9_armhf.deb
wget -N https://ftp.debian.org/debian/pool/non-free/s/svox/libttspico0_1.0+git20130326-9_armhf.deb

# Ajout du dépôt log2ram
echo "deb https://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
wget -qO - https://azlux.fr/repo.gpg.key | apt-key add -

# Installation des paquets
debut_paquets=$(date +%s)
apt update
apt dist-upgrade -y
echo "Fin de mise à jour du système." | tee -a "$fichierlog"

paquets_introuvables=""

for paquet in ${paquets_base}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  if [ "$paquet" = "samba" ]; then
        temps_pause_on=$(date +%s)
	    apt install -y "$paquet"
		temps_pause_off=$(date +%s)
	  else 
	  	apt install -y "$paquet"
	  fi
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des paquets de base
--------------------------------------" | tee -a "$fichierlog"

# Installation des paquets libttspico
apt install -y libttspico-data
dpkg -i libttspico0_1.0+git20130326-9_armhf.deb
dpkg -i libttspico-utils_1.0+git20130326-9_armhf.deb

for paquet in ${paquets_educatifs}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  apt install -y "$paquet"
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des Paquets éducatifs
----------------------------------------" | tee -a "$fichierlog"

for paquet in ${paquets_primtux}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  apt install -y "$paquet"
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des paquets PrimTux
--------------------------------------" | tee -a "$fichierlog"

for paquet in ${paquets_rpi}
do
	existe=$(apt-cache show "$paquet")
	if [ -n "$existe" ]; then
	  apt install -y "$paquet"
	else 
	  paquets_introuvables="$paquets_introuvables $paquet"
	fi
done
echo "Fin d'installation des paquets RPi
----------------------------------" | tee -a "$fichierlog"

# Installation des applications jLoDB poufpouf
mkdir /tmp/sources-primtux/jlodb
for fichier in ${paquets_jlodb}; do
   wget -N -P /tmp/sources-primtux/jlodb "$sources_jlodb$fichier"
   dpkg -i "/tmp/sources-primtux/jlodb/$fichier"
done

apt install log2ram

apt --fix-broken install -y
fin_paquets=$(date +%s)

temps_pause=$(($temps_pause_off-$temps_pause_on))
temps_paquets=$(($fin_paquets-$debut_paquets))
temps_paquets=$(($temps_paquets-$temps_pause))
temps_pause=$(echo $temps_pause |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
temps_paquets=$(echo $temps_paquets |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')
echo "Fin des opérations d'installation des paquets en $temps_paquets" | tee -a "$fichierlog"

# Paramétrage du gestionnaire réseaux
if ! grep "denyinterfaces wlan0" /etc/dhcpcd.conf; then
  echo "denyinterfaces wlan0" >>/etc/dhcpcd.conf
fi
if ! grep dhcp=internal /etc/NetworkManager/NetworkManager.conf; then
  sed -i '/plugins=ifupdown,keyfile/ adhcp=internal' /etc/NetworkManager/NetworkManager.conf
fi
sed -i 's/managed=false/managed=true/' /etc/NetworkManager/NetworkManager.conf

if [ -n "$paquets_introuvables" ]; then
   echo "Paquets non disponibles dans les dépôts :" >>"$fichierlog"
   echo "$paquets_introuvables" >>"$fichierlog"
fi

# Configuration de PrimTux 7
mkdir /tmp/sources-primtux/hooks
tar --keep-directory-symlink --overwrite -xzvf /tmp/sources-primtux/"$archive_chroot" -C /
tar xzvf /tmp/sources-primtux/"$archive_hooks" -C /tmp/sources-primtux/hooks
rm /tmp/sources-primtux/"$archive_chroot" /tmp/sources-primtux/"$archive_hooks"

# Copie de la configuration du thème xfce4
tar xzvf /tmp/"$archive_xfce" -C /tmp
rm /tmp/"$archive_xfce"
cp -R /tmp/xfce4-config/01-mini/xfce-perchannel-xml /etc/skel-mini/.config/xfce4/xfconf
cp -R /tmp/xfce4-config/02-super/xfce-perchannel-xml /etc/skel-super/.config/xfce4/xfconf
cp -R /tmp/xfce4-config/03-maxi/xfce-perchannel-xml /etc/skel-maxi/.config/xfce4/xfconf
cp -R /tmp/xfce4-config/administrateur/xfce-perchannel-xml /etc/skel/.config/xfce4/xfconf
rm -rf /tmp/xfce4-config

# Copie de la configuration d'Audacity
tar xzvf /tmp/"$archive_audacity" -C /etc/skel
tar xzvf /tmp/"$archive_audacity" -C /etc/skel-mini
tar xzvf /tmp/"$archive_audacity" -C /etc/skel-super
tar xzvf /tmp/"$archive_audacity" -C /etc/skel-maxi
sed -i 's#^TempDir=.*$#TempDir=/var/tmp/audacity-01-mini#' /etc/skel-mini/.audacity-data/audacity.cfg
sed -i 's#^TempDir=.*$#TempDir=/var/tmp/audacity-02-super#' /etc/skel-super/.audacity-data/audacity.cfg
sed -i 's#^TempDir=.*$#TempDir=/var/tmp/audacity-03-maxi#' /etc/skel-maxi/.audacity-data/audacity.cfg
rm -rf /tmp/"$archive_audacity"

# Configuration de roxterm
tar xvf /tmp/$(basename "$config_roxterm") -C /etc/skel/.config/roxterm.sourceforge.net
rm /tmp/$(basename "$config_roxterm")

# Suppression de l'utilisateur pi
userdel -r pi

# Modification de la configuration du RPi pour sddm
sed -i 's/^dtoverlay/#dtoverlay/' /boot/config.txt
sed -i '/\[all\]/ adtoverlay=vc4-fkms-v3d' /boot/config.txt

# Application des scripts de configuration de PrimTux
sh /tmp/sources-primtux/hooks/1000.useradd.hook.chroot
sh /tmp/sources-primtux/hooks/1001.sddm.hook.chroot
sh /tmp/sources-primtux/hooks/1004.rep-public.hook.chroot

# Copie les répertoires de chaque utilisateur
rsync -av /etc/skel/ /home/administrateur
rsync -av /etc/skel-mini/ /home/01-mini
rsync -av /etc/skel-super/ /home/02-super
rsync -av /etc/skel-maxi/ /home/03-maxi

# on donne à chaque répertoire utilisateur ses droits
chown -R 01-mini:01-mini /home/01-mini
chown -R 02-super:02-super /home/02-super
chown -R 03-maxi:03-maxi /home/03-maxi
chown -R administrateur:administrateur /home/administrateur
chmod -R 2777 /home/administrateur/Public
# Change les shells utilisateurs
chsh -s /bin/bash administrateur
chsh -s /bin/false 01-mini
chsh -s /bin/false 02-super
chsh -s /bin/false 03-maxi

# Protection des environnements de bureau des élèves
chown -R administrateur:administrateur /home/01-mini/.config/xfce4
chown -R administrateur:administrateur /home/02-super/.config/xfce4
chown -R administrateur:administrateur /home/03-maxi/.config/xfce4/
chmod -R 755 /home/01-mini/.config/xfce4
chmod -R 755 /home/02-super/.config/xfce4
chmod -R 755 /home/03-maxi/.config/xfce4

# Paramétrage audacity
mkdir /var/tmp/audacity-01-mini
chown 01-mini:01-mini /var/tmp/audacity-01-mini
chmod 644 /var/tmp/audacity-01-mini
mkdir /var/tmp/audacity-02-super
chown 02-super:02-super /var/tmp/audacity-02-super
chmod 644 /var/tmp/audacity-02-super
mkdir /var/tmp/audacity-03-maxi
chown 03-maxi:03-maxi /var/tmp/audacity-03-maxi
chmod 644 /var/tmp/audacity-03-maxi
mkdir /var/tmp/audacity-administrateur
chown administrateur:administrateur /var/tmp/audacity-administrateur
chmod 644 /var/tmp/audacity-administrateur

# Nettoyage des dossiers .wine inutiles sur RPi
rm -rf /home/{01-mini,02-super,03-maxi,administrateur}/.wine

# On nettoie !
rm -rf /tmp/sources-primtux
rm -rf tmp
rm libttspico0_1.0+git20130326-9_armhf.deb
rm libttspico-utils_1.0+git20130326-9_armhf.deb

# Suppression des lanceurs inutiles
rm /usr/share/applications/wine.desktop
rm -rf /usr/share/applications/screensavers

# Vérification des paquets installés
paquets_jlodb=$(echo "$paquets_jlodb" | sed 's/_19.10.0_all.deb//g')
paquets=$(echo "$paquets_base $paquets_educatifs $paquets_primtux $paquets_rpi $paquets_jlodb")
dpkg -l | sed -e '1,6d' -e "s/[ ][ ]*/#/g" | cut -d '#' -f 2 > /tmp/installes.txt
echo "Paquets manquants :" >> "$fichierlog"
for paquet in ${paquets}
do
   if ! grep "$paquet" /tmp/installes.txt > /dev/null; then
      echo "$paquet" >> "$fichierlog"
   fi
done
rm /tmp/installes.txt

# Indication de version de l'OS
echo "$version" >/etc/primtux_version

# Nettoyage du fichier de suivi des opérations
sed -i '/[1-9].*K \.\.\.\.*/ d' "$fichierlog"
fin=$(date +%s)

# Calul des temps de construction
temps_total=$(($fin-$debut))
temps_total=$(($temps_total-$temps_pause))
temps_total=$(echo $temps_total |awk '{printf "%02d:%02d:%02d\n",$1/3600,$1%3600/60,$1%60}')

echo "Les opérations sont terminées et ont duré au total $temps_total dont :
  - $temps_telechargement en temps de téléchargement des fichiers de configuration ;
  - $temps_paquets en temps de téléchargement et d'installation de paquets." | tee -a "$fichierlog"
echo "
Fin des opérations" >> "$fichierlog"
echo "Un fichier du déroulement des opérations et des erreurs a été créé en $fichierlog
Veuillez redémarrer le système." 

exit 0
